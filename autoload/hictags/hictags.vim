let s:tags_file = 'tags'
let s:tags_read_ts = 0

function! s:ReadTags()
  let l:tags_file_ts = getftime(s:tags_file)
  if s:tags_read_ts >=# l:tags_file_ts
    return
  endif

  for raw_tag in readfile(s:tags_file)
    if match(raw_tag, '!') ==# 0
      continue
    endif

    let l:tag_parts = split(raw_tag, '\t')

    let l:tag = l:tag_parts[0]
    let l:filename = l:tag_parts[1]
    let l:tag_regex = l:tag_parts[2][2:-5]
    let l:tag_type = l:tag_parts[3]

    let l:tags_for_file = get(g:hictags#file_to_tags, l:filename, [])
    call add(l:tags_for_file, [l:tag, l:tag_regex, l:tag_type])
    let g:hictags#file_to_tags[l:filename] = l:tags_for_file
  endfor

  let s:tags_read_ts = l:tags_file_ts
endfunction

function! hictags#hictags#Hictags(filename)
  call s:ReadTags()

  for [tag, regex, type] in get(g:hictags#file_to_tags, a:filename, [])
    if index(['f', 'p'], type) ==# -1
      continue
    endif

    if match('/\M^' . regex . '/', '\M' . tag) ==# -1
      continue
    endif

    let l:tag_start = strridx(regex, tag)
    let l:tag_end = l:tag_start + len(tag)

    let l:command = 'syntax match piterFunction ' . '/\M^' . regex . '/'
    let l:command .= 'hs=s+' . l:tag_start . ',ms=s'
    let l:command .= ',he=s+' . l:tag_end  . ',me=s+' . l:tag_end
    let l:command .= ' contains=.*Type,.*Structure,@.*ParenGroup,.*StorageClass'
    execute l:command
  endfor

  hi link piterFunction Function
endfunction

