" hictags.vim - Syntax highlighting based on ctags
" Author:       Piotr Łusakowski
" Version:      0.1
" License:      MIT

if exists('g:loaded_highlight_ctags_plugin') || &compatible || v:version < 700
  finish
endif
let g:loaded_highlight_ctags_plugin = 1

let s:cpo_save = &cpo
set cpo&vim

function! s:Init()
  let g:hictags#options = {}
  let g:hictags#file_to_tags = {}
endfunction

call s:Init()

command! -nargs=0 Hictags :call hictags#hictags#Hictags(expand('%:.'))

let &cpo = s:cpo_save
unlet s:cpo_save
